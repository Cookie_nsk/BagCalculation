﻿function InitJS() {
    window.guids = new Array();
    getTasksList();
}

function validateIsNumber(sender) {
    var inputVal = $(sender).val();
    var digit = new RegExp(/^[0-9]+$/i);
    return digit.test(inputVal);
}
function isDigit(sender) {
    if (!validateIsNumber(sender)) {
        $(sender).val("");
    }
}
function validateInput(sender) {
    var ctrls = $('#' + sender).find('[data-isRequired="true"]');
    var errors = 0;
    ctrls.each(function () {
        if (this.value.length === 0 || this.value.length > 255) {
            addClassValid(this, false);
            errors++;
        }
        else {
            addClassValid(this, true);
        }
    });
    return errors === 0;
}
function addClassValid(sender, isValid) {
    var elem = $(sender);
    if (isValid) {
        elem.addClass("is-valid");
        elem.removeClass("is-invalid");
        elem.parent().parent().find(".invalid-feedback").hide();
    }
    else {
        elem.addClass("is-invalid");
        elem.removeClass("is-valid");
        elem.parent().parent().find(".invalid-feedback").show();
    }
}

//Api calls
function createTask(div, divCap, divQuant) {
    if (validateInput(div)) {
        $.ajax({
            type: "PUT",
            url: "/api/MainApi/CreateTask/" + $("#" + divCap).val() + "/" + $("#" + divQuant).val(),
            complete: getTasksList
        });
        $("#tasksList-tab").click();
    }
}
function cancelTask(guid) {
    $.ajax({
        type: "DELETE",
        url: "/api/MainApi/DeleteTask/" + guid,
        success: function (data) {
            removeFromGuidArray(guid);
            getTasksList();
        }
    });
}
function getTasksList() {
    $.ajax({
        type: "GET",
        url: "/api/MainApi/GetTasks",

        success: function (data) {
            $("#tblTasks tbody tr").remove();
            for (var i = 0; i < data.length; i++) {

                var tRow = "<tr>" +
                    "<td id=\"tdViewResults_" + data[i].Guid + "\" style=\"display:none\">" + "<a href=\"#\" data-toggle=\"modal\" data-target=\"#modalResults\" onclick=\"showResults(\'" + data[i].Guid + "\')\">" + data[i].Guid + "</a></td>" +
                    "<td id=\"tdName_" + data[i].Guid + "\">" + data[i].Guid + "</td>" +
                    "<td id=\"tdState_" + data[i].Guid + "\">" + data[i].StateString + "</td>" +
                    "<td id=\"tdPc_" + data[i].Guid + "\">" + data[i].CompletePercentageString + "%" + "</td>";

                if (data[i].CompletePercentage < 100) {
                    tRow += "<td>" + "<button type=\"button\" id=\"btnCancel_" + data[i].Guid + "\" class=\"btn btn-outline-dark  btn-sm\" onclick=\"cancelTask('" + data[i].Guid + "') \">Отменить задачу</button></td>";
                }
                else {
                    tRow += "<td></td>";
                }
                tRow += "</tr>";

                $('#tblTasks tbody').append(tRow);
                if (parseInt(data[i].CompletePercentage) < 100 && data[i].State === 0/*InProgress*/) {
                    addToGuidArray(data[i].Guid);
                }
                if (parseInt(data[i].CompletePercentage) >= 100) {
                    $("#tdViewResults_" + data[i].Guid).show();
                    $("#tdName_" + data[i].Guid).hide();
                }
            }
        }
    });
}
function showResults(guid) {
    $.ajax({
        type: "GET",
        url: "/api/MainApi/GetResults/" + guid,

        success: function (data) {

            $("#tblItemsResults tbody tr").remove();
            $("#tblItemsSource tbody tr").remove();

            $("#divTotalWeight").html("Вместимость: " + data.BagVolume);
            $("#divOverallVolume").html("Занято места: " + data.OverallVolume);
            $("#divTotalCost").html("Общая ценность: " + data.BagOverallCost);
            $("#divUsedItems").html("Использовано предметов " + data.ResultSet.length + " из " + data.InputConditions.length);

            $(data.ResultSet).each(function () {
                var tRow = "<tr>" +
                    "<td>" + this.Name + "</td>" +
                    "<td>" + this.Weight + "</td>" +
                    "<td>" + this.Cost + "</td>" + "</tr>";

                $('#tblItemsResults tbody').append(tRow);
            });

            $(data.InputConditions).each(function () {
                var tRow = "<tr>" +
                    "<td>" + this.Name + "</td>" +
                    "<td>" + this.Weight + "</td>" +
                    "<td>" + this.Cost + "</td>" + "</tr>";

                $('#tblItemsSource tbody').append(tRow);
            });


        }
    });
}
//Api calls 
function removeFromGuidArray(guid) {
    if (window.guids.indexOf(guid) !== -1)
        window.guids.splice(window.guids.indexOf(guid), 1);
}
function addToGuidArray(guid) {
    if (window.guids.indexOf(guid) === -1)
        window.guids.push(guid);
}
//SignalR section
$(function () {
    var notificationhub = $.connection.notificationHub;
    notificationhub.client.displayMessage = function (message) {

        var guid = message.split(";")[0];
        var completePercentage = message.split(";")[1];
        var stateString = message.split(";")[2];

        $("#tdPc_" + guid).html(completePercentage + '%');
        $("#tdState_" + guid).html(stateString);

        if (parseInt(completePercentage) >= 100) {
            removeFromGuidArray(guid);
            $("#btnCancel_" + guid).hide();
            $("#tdViewResults_" + guid).show();
            $("#tdName_" + guid).hide();
        }

    };
    $.connection.hub.start();

});