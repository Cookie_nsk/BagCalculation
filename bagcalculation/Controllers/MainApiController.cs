﻿using BagCalculation.Helpers;
using Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BagCalculation.Controllers
{
    public class MainApiController : ApiController
    {
        public IEnumerable<Task> GetTasks()
        {
            return TasksHelper.Tasks;
        }
        [HttpPut]
        public void CreateTask(int param, int param2)
        {
            TasksHelper.CreateNewTask(param, param2);
        }
        public bool DeleteTask(string param)
        {
            return TasksHelper.CancelTask(param);
        }
        public Task GetResults(string param)
        {
            return TasksHelper.Tasks.First(x => x.Guid.Equals(param));
        }
    }
}
